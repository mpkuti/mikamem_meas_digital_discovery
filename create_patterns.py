#!/usr/bin/python3

import sys
import csv
import binascii
import random
import bitstring


# CHECK THAT PYTHON 3 IS USED
if sys.version_info < (3, 0):
    sys.stdout.write("NOTE: Python 3.x is preferred, not Python 2.x\n")
#    sys.stdout.write("ERROR: Requires Python 3.x, not Python 2.x\n")
#    sys.exit(1)


print('----------  CONSTANTS  ----------')
PROJECT_DIR = './'
OUTPUT_DIR = PROJECT_DIR + 'patterns/'
print( "Project dir:\t", PROJECT_DIR )
print( "Output dir:\t", OUTPUT_DIR )

ADDRESS_MAX = 127
ADDRESS_MIN = 0
ADDRESS_RANGE = range(ADDRESS_MIN,ADDRESS_MAX+1)

#CLK_HI_INT = int(r'00010000',2)
#CLK_LO_INT = int(r'00000000',2)
#CLK_HI_HEX = hex(CLK_HI_INT)
#CLK_LO_HEX = hex(CLK_LO_INT)
#print( CLK_HI_HEX )
#print( CLK_LO_HEX )


print('----------  VARIABLES  ----------')
# BitArray from bitstring package
ctrl_word    = bitstring.BitArray('0b00000011')
clk_word     = bitstring.BitArray('0b00000000')
din_hi_word  = bitstring.BitArray('0b00000000')
din_lo_word  = bitstring.BitArray('0b00000000')
addr_lo_word = bitstring.BitArray('0b00000000')
# CREATE EMPTY LISTS
din_hi_pattern = []
addr_pattern = []
din_lo_pattern = []
ctrl_pattern = []
rst_pattern = []
ctrl_clk_pattern = []


print('----------  FUNCTIONS  ----------')

# cen and wen are active-low
def chip_enable():
    ctrl_word[7] = 0
    return

def chip_disable():
    ctrl_word[7] = 1
    return

def write_enable():
    ctrl_word[6] = 0
    return

def write_disable():
    ctrl_word[6] = 1
    return

def clk_rise():
    clk_word[3] = 1
    return

def clk_fall():
    clk_word[3] = 0
    return

def set_din_to_zeros():
    global din_hi_word
    global din_lo_word
    din_hi_word = bitstring.BitArray( '0b00000000' )
    din_lo_word = bitstring.BitArray( '0b00000000' )
    return

def set_din_to_ones():
    global din_hi_word
    global din_lo_word
    din_hi_word = bitstring.BitArray( '0b11111111' )
    din_lo_word = bitstring.BitArray( '0b11111111' )
    return

def generate_random_din():
    global din_hi_word
    global din_lo_word
    random_int = random.randint(0,255)
    din_hi_word = bitstring.BitArray( uint=random_int, length=8 )
    random_int = random.randint(0,255)
    din_lo_word = bitstring.BitArray( uint=random_int, length=8 )
    return 

def set_addr_word( addr_int = 0 ):
    # 0 <= addr_int <= 127
    if addr_int in ADDRESS_RANGE:
        global addr_word
        addr_word = bitstring.BitArray( uint=addr_int, length=8 )
        return
    else
        return

def append2lists( index ):
    datain = '11110000'
    din_hi_pattern.extend(datain)
    return


def fill_memory_with_random_data( count = (ADDRESS_MAX+1) ):
    # ENABLE WRITE
    chip_enable()
    write_enable()
    # GENERATE THE PATTERNS
    for i in range( count ):
        generate_random_din()
        din_hi_pattern.extend( din_hi_word.bin )
        din_lo_pattern.extend( din_lo_word.bin )
        set_addr_word( i )
        addr_pattern.extend( addr_word.bin )
        ctrl_pattern.extend( ctrl_word.bin )
    return


print('----------  MAIN  ----------')

# WRITE DATA TO SRAM

chip_enable()
write_enable()

for i in range(4):
    generate_random_din()
    din_hi_pattern.extend( din_hi_word.bin )
    din_lo_pattern.extend( din_lo_word.bin )
    set_addr_word( i )
    addr_pattern.extend( addr_word.bin )
    ctrl_pattern.extend( ctrl_word.bin )

# READ DATA FROM SRAM

chip_enable()
write_disable()
set_din_to_zeros()

for i in range(4):
    set_addr_word( i )
    addr_pattern.extend( addr_word.bin )
    ctrl_pattern.extend( ctrl_word.bin )


def list2csv( pattern_list, filename ):
    with open(filename, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(pattern_list)
    return

# WRITE LISTS TO CSV FILE
list2csv(din_hi_pattern, OUTPUT_DIR + 'DIN_HI.csv')
list2csv(addr_pattern, OUTPUT_DIR + 'ADDR.csv')
list2csv(din_lo_pattern, OUTPUT_DIR + 'DIN_LO.csv')
list2csv(ctrl_pattern, OUTPUT_DIR + 'CTRL.csv')
list2csv(rst_pattern, OUTPUT_DIR + 'RST.csv')
list2csv(ctrl_clk_pattern, OUTPUT_DIR + 'CTRL_CLK.csv')
