init:
	sudo apt install python3
	sudo apt install python3-pip
	pip3 install -r python3_requirements.txt

.PHONY: init
